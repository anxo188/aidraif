# aiDraif

Proyecto práctico de la asignatura TSW

# Funcionalidades 
https://cursos.faitic.uvigo.es/tema1920/claroline/document/goto/index.php/practicas/Descripcion_del_proyecto_Web.htm?cidReq=O06G150V01970

# Evaluación

La evaluación se llevará a cabo teniendo en cuenta los siguientes criterios:

|  |   Se evaluará que... | Puntuación |
| ------ | ------ | ------ |
| HTML/CSS 	| División del espacio con DIVs/CSS y no con tablas HTML. 	| 30% | 
| HTML/CSS 	| La información relativa al estilo va en el CSS, no en el HTML.|  	30%| 
| HTML/CSS 	| Se ve correctamente en la última versión de Internet Explorer, Chrome, Firefox (al menos dos de ellos).|  	20%| 
| HTML 	HTML | correcto y cuidado. Uso de meta-tags/cabeceras donde proceda.|  	10%| 
| Eficiencia 	| Empleo de colores e imágenes tratando de transmitir los mínimos bytes posibles para el diseño escogido (dependerá de cada diseño).|  	10%| 
| Estética 	| Combinación de colores agradable, novedad visual, iconografía cuidada, fuentes adecuadas, legibilidad, etc.|  	+20%| 
| Responsive |  	Que la web se adapte y sea agradable en función del ancho de la pantalla, por lo menos entre 320px y 1920px|  	+20%| 